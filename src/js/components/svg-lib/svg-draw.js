import { __localPosition, __dispatchEvent } from '../../lib/utils';

// import SVG from '../svg';
import SVGDragNDrop from './svg-dragndrop';
import SVG from '../svg';

export default class SVGDraw {
  // static selectPath(svg) {
  //   // https://bl.ocks.org/mbostock/8027637
  // }

  static renderPath($svg, color, callback) {
    if (d3.event.sourceEvent.target.nodeName === 'image') {

      const line = d3.line()
        // removed smooth curve to get rid of loong decimals
        // see https://github.com/d3/d3-path/issues/10
        .curve(d3.curveLinear); // _dragStarted

      const format = d3.format('.0f');
      const d = d3.event.subject;
      // origin
      let x0 = format(d3.event.x);
      let y0 = format(d3.event.y);

      const id = `${$svg.id}_path-${x0}-${y0}`;

      const active = d3.select(`#${$svg.id} .svg-view`).append('svg:path')
        // .attr('marker-start', 'url('#marker-circle-blue')')
        // .attr('marker-end', 'url('#marker-arrow-blue')')
        .attr('data-svg-path', '')
        .attr('class', `svg-path ${color}`)
        .attr('id', id)
        .datum(d);

      // github.com/d3/d3-drag#event_on
      d3.event
        .on('drag', () => {
          // current position
          const x1 = format(d3.event.x);
          const y1 = format(d3.event.y);
          // distance from origin
          const dx = x1 - x0;
          const dy = y1 - y0;

          // if the distance of drag event is too large, make new origin
          if ((dx * dx) + (dy * dy) > 300) d.push([x0 = x1, y0 = y1]);
          // smooth line, correct last point
          else d[d.length - 1] = [x1, y1];

          active.attr('d', line);
        })
        .on('end', () => {
          const $thisPath = d3.select(`#${id}`);
          if ($thisPath) {
            if ($thisPath.attr('d')) {
              if ($thisPath.attr('d').length < 40) {
                $thisPath.remove();
              } else {
                // dispatch event that svg has changed
                __dispatchEvent(document, 'svg.change', {}, {
                  svg: {
                    id: $svg.id,
                    task: 'create',
                    node: $thisPath.node(),
                    nodeName: $thisPath.node().nodeName,
                    contextMenu: false,
                  },
                });
              }
            } else if (!$thisPath.attr('d')) {
              $thisPath.remove();
            }
          }

          callback();
        });

      // add events to element
      SVGDraw.pathEventsManager($svg, d3.select(`#${id}`).node());
    }
  }

  // Draw a SVG circle on click
  static createCircle($svg, color) {
    const svgGroup = d3.select($svg).select('.svg-view');
    //d3.select(`#${$svg.id} .svg-view`);
    // const canvas = document.querySelector(`#${$svg.id} .svg-view`);
    const localPosition = __localPosition($svg);
    const id = `${$svg.id}_circle-${localPosition.x}-${localPosition.y}`;

    // circle wrapper
    const foreignObject = svgGroup.append('svg:foreignObject')
      .attr('data-svg-circle', '')
      .attr('id', id)
      .attr('x', localPosition.x)
      .attr('y', localPosition.y)
      .attr('data-svg-resolution', `${svgGroup.attr('width')}, ${svgGroup.attr('height')}`)
      .attr('data-svg-type', 'circle')
      .attr('width', 1)
      .attr('height', 1);

    const circle = foreignObject.append('xhtml:div')
      .attr('class', `svg-circle ${color}`);

    const $element = document.querySelector(`#${id}`);

    const $node = $element.firstChild;
    // add events to element
    SVGDraw.circleEventsManager($svg, $element, $node, 'circle');

    // dispatch event that svg has changed
    __dispatchEvent(document, 'svg.change', {}, {
      svg: {
        id: $svg.id,
        task: 'create',
        node: $element,
        nodeName: $element.nodeName,
        contextMenu: false,
      },
    });
  }

  static pathEventsManager($svg, $element) {
    d3.select(`#${$element.id}`).on('click', () => {
      // dispatch event that svg has changed
      __dispatchEvent(document, 'svg.change', {}, {
        svg: {
          id: $svg.id,
          task: 'remove',
          node: d3.event.target,
          nodeName: d3.event.target.nodeName,
          contextMenu: false,
        },
      });

      d3.select(`#${$element.id}`).remove();

      d3.event.preventDefault();
    }, false);
  }

  static circleEventsManager($svg, $element, $node, type) {
    const $canvas = document.querySelector(`#${$svg.id} .svg-view`);

    // TODO:
    // standalone function
    // same for all draggable svg elements
    let elementPosition = [
      $element.getAttribute('x'),
      $element.getAttribute('y'),
    ];
    let diff = {};

    const selection = d3.selectAll(`#${$element.id} > div`);
    d3.select(selection.nodes()[0]).on('click', () => {

      // dispatch event that svg has changed
      __dispatchEvent(document, 'svg.change', {}, {
        svg: {
          id: $svg.id,
          task: 'remove',
          node: $element,
          nodeName: $element.nodeName,
          contextMenu: false,
        },
      });

      // dispatch event that drop item was deleted
      const idParts = $element.id.split('_');
      if (idParts[idParts.length - 1] === 'dropItem') {
        __dispatchEvent(document, 'svg.drop.delete', {}, {
          item: {
            id: idParts[1],
            $object: $element.firstChild,
          },
        });
      }

      d3.event.target.closest('foreignObject').remove();
    })
      .call(d3.drag()
        .container($canvas)
        .subject($element)
        .on('start', () => {
          diff = {
            x: d3.event.x - elementPosition[0],
            y: d3.event.y - elementPosition[1],
          };

          SVGDragNDrop.start($element);
        })
        .on('drag', () => {
          SVGDragNDrop.dragged($element, type, diff, $node);
        })
        .on('end', () => {
          const oldXCoordinate = elementPosition[0];
          const oldYCoordinate = elementPosition[1];
          let taskType = 'move';

          SVGDragNDrop.end($element, type, diff,  $node);

          elementPosition = [
            $element.getAttribute('x'),
            $element.getAttribute('y'),
          ];

          // if clone is changed, then dispatch create event
          if (SVG.removeCloneConnectionManager($element)) taskType = 'create';

          if (elementPosition[0] !== oldXCoordinate || elementPosition[1] !== oldYCoordinate) {
            // dispatch event that svg has changed
            __dispatchEvent(document, 'svg.change', {}, {
              svg: {
                id: $svg.id,
                task: taskType,
                node: $element,
                nodeName: $element.nodeName,
                contextMenu: false,
              },
            });
          }
        }));
  }
}
